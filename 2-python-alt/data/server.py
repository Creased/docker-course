#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import asyncio

from config import SERVER_HOST, SERVER_PORT
from utils import logger

logger.name = 'server'

async def handle_client(reader, writer):
    peername = writer.get_extra_info('peername')
    logger.info(f'Connection from {peername[0]!s}:{peername[1]:d}.')

    message = await reader.read(100)
    logger.info(f'Received {message.decode()!r} from {peername[0]!s}:{peername[1]:d}.')

    message = 'Pong\n'.encode()
    logger.info(f'Sending {message.decode()!r} to {peername[0]!s}:{peername[1]:d}...')
    writer.write(message)
    await writer.drain()

    writer.close()
    await writer.wait_closed()
    logger.info(f'Connection closed for {peername[0]!s}:{peername[1]:d}.')

def main():
    loop = asyncio.get_event_loop()
    coro = asyncio.start_server(client_connected_cb=handle_client,
                                loop=loop,
                                host='0.0.0.0',
                                port=SERVER_PORT)
    server = loop.run_until_complete(coro)

    addr = server.sockets[0].getsockname()
    logger.info(f'Serving on {addr[0]!s}:{addr[1]:d} (press ctrl-c to stop)')

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info(f'Killing the server...')

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()

if __name__ == '__main__':
    main()
