import os

SERVER_HOST = os.environ.get('SERVER_HOST', 'pong-server')
SERVER_PORT = int(os.environ.get('SERVER_PORT', 42))