# Formation Docker - projets d'exemple

## 1 - Code C

Ce projet illustre la composition d'images en deux phases (multistage build) :

 * Lors de la phase de build, le programme est compilé en utilisant la *GNU toolchain* sur Debian Stretch
 * Le binaire compilé est copié sur une image vide (*scratch*)

Le programme exécuté affiche un "Hello, World!" avec son PID entre crochets et stoppe son exécution.

## 2 - Python

Ce projet illustre l'exploitation d'une même image pour deux conteneurs différents (seule la commande de démarrage change). Par ailleurs, on retrouve l'utilisation de :

 * Volumes en lecture seule
 * Redémarrage du conteneur en cas d'extinction du processus principal
 * La communication et la liaison inter-conteneurs
 * L'utilisation de healthcheck pour contrôler le fonctionnement réel du programme principal (même s'il n'est pas éteint, le processus principal ne répond pas forcément)
 * La chaîne de dépendances
 * L'exploitation de variables d'environnement

Le service exécuté procède à des échanges ping-pong entre deux conteneurs.

## 3 - Load Balancing (TD)

Ce projet illustre l'exploitation avancée de ressources partagées entre différents conteneurs&nbsp;:

 * Architecture client / serveur + consumer avec load balancing
    * 1 client : envoi de données à un serveur (uptime)
    * 2 serveurs : réception de données, stockage dans un FIFO
    * 1 consumer : lecture du FIFO (cat)
 * Réseaux internes et réseaux pontés (*bridge*)
 * Partage d'un named pipe FIFO (First in, First out) pour compatibilité multi-consumer
 * Load balancing automatique basé sur la résolution de noms (DNS interne au *Docker daemon*)
 * Partage de ressources en lecture seule
