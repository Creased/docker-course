#!/bin/bash
mkfifo -m a=rw data/info

docker-compose pull
docker-compose build
docker-compose push
docker-compose up -d

source .env
screen -dmS ${COMPOSE_PROJECT_NAME} docker-compose logs -f

