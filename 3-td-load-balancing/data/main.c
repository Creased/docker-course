#define _OPEN_SYS
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
    char * device_name = "info";
    int retval;

    fprintf(stdout, "[?] Creating %s fifo...\n", device_name);
    if (mkfifo(device_name, 0666) == -1) {
        fprintf(stderr, "[!] Failed with error: %s\n", strerror(errno));
        retval = EXIT_FAILURE;
    } else {
        fprintf(stderr, "[+] %s\n", strerror(errno));
        retval = EXIT_SUCCESS;
    }

    return retval;
}
