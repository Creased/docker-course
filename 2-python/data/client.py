#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import time
import asyncio

from config import SERVER_HOST, SERVER_PORT
from utils import logger

logger.name = 'client'

async def ping_pong(loop):
    reader, writer = await asyncio.open_connection(SERVER_HOST, SERVER_PORT,
                                                   loop=loop)

    peername = writer.get_extra_info('peername')
    message = 'Ping\n'.encode()
    logger.info(f'Sending {message.decode()!r} to {peername[0]!s}:{peername[1]:d}...')
    writer.write(message)
    await writer.drain()

    message = await reader.read(100)
    logger.info(f'Received {message.decode()!r} from {peername[0]!s}:{peername[1]:d}.')

    writer.close()
    await writer.wait_closed()
    logger.info(f'Connection closed.')

def main():
    try:
        while True:
            try:
                loop = asyncio.get_event_loop()
                coro = loop.create_task(ping_pong(loop))

                loop.run_until_complete(coro)

                time.sleep(2)
            except ConnectionRefusedError:
                logger.error(f'Connection failed to the server! Retrying in 5 seconds...')
                time.sleep(5)
    except KeyboardInterrupt:
        logger.info(f'Killing the client...')

    loop.close()

if __name__ == '__main__':
    main()
