#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char ** argv) {
    int retval;

    if (argc == 1) {
        fprintf(stdout, "%s[%d]: Hello, World!\n", argv[0], getpid());
        retval = EXIT_SUCCESS;
    } else {
        fprintf(stderr, "You should not pass arguments to this program!\n");
        retval = EXIT_FAILURE;
    }

    return retval;
}
